# A GOOD SNOWMAN IS HARD TO BUILD

Here we go: develop an game with triple-store (here: stardog) and VueJS client.

## TODO

- implement moves on FreeCell in `database.js` for each direction
- call the right movement function in each controller
- improve moves' request to shift snowball over FreeCell
- improve board to handle multiple class for one Cell
- improve moves' request to shift snowball over FreeCell or over another snowball

## RUN THE APPLICATION

This application use Docker to deploy Stardog database. First step is to use `docker-compose up` to create the container and add
the [ontology](./ontology.ttl) to stardog.

Next, run `npm run serve` and open [Snowman](http://localhost:8080). Otherwise, you can use `npm run build` and modify `docker-compose.yml` to add another container (like httpd or nginx) to serve the dist build in the way you want.