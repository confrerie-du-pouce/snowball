const { Connection, query } = require("stardog");

const conn = new Connection({
  username: "admin",
  password: "admin",
  endpoint: "http://localhost:5820"
});

const prefix = "PREFIX : <http://clement/snowman#>";

const board = `${prefix}
SELECT DISTINCT *
WHERE {
    ?c a ?concept .
    ?c :hasX ?x .
    ?c :hasY ?y .
    FILTER (
        ?concept = :CellFree ||
        ?concept = :CellPlayer ||
        ?concept = :SmallSnowball ||
        ?concept = :MediumSnowball ||
        ?concept = :BigSnowball
        )
} ORDER BY ?c`;

async function makeMove(dir) {
  await execute(moveSnowball(dir));
  await execute(movePlayer(dir));
}

function movePlayer(dir) {
  return `${prefix}
DELETE {
    ?player a :CellPlayer .
    ?target a :CellFree
} INSERT {
    ?player a :CellFree .
    ?target a :CellPlayer
}
WHERE {
    ?player a :CellPlayer .
    ?player :has${dir} ?target .
    ?target a :CellFree
}`;
}

function moveSnowball(dir) {
  return `${prefix}
DELETE {
    ?snowball a ?size .
    ?next a :CellFree
} INSERT {
    ?snowball a :CellFree .
    ?next a ?size
}
WHERE {
    ?player a :CellPlayer .
    ?player :has${dir} ?snowball .
    ?snowball a ?size FILTER (?size = :SmallSnowball || ?size = :MediumSnowball || ?size = :BigSnowball) .
    ?snowball :has${dir} ?next .
    ?next a ?type FILTER (
        ?type = :CellFree ||
        (?type = :BigSnowball && ?size = :MediumSnowball && ?type != :MediumSnowball) ||
        (?type = :MediumSnowball && ?size = :SmallSnowball && ?type != :SmallSnowball)
    )
}`;
}

async function cleanDatabase() {
  await execute(`${prefix}
  DELETE {
    ?cell a ?type
  } INSERT {
    ?cell a :CellFree
  } WHERE {
    ?cell a :Cell .
    ?cell a ?type
    FILTER (
      ?type = :CellPlayer ||
      ?type = :SmallSnowball ||
      ?type = :MediumSnowball ||
      ?type = :BigSnowball
    ) 
  }`);
}

function getNewId(seen = {}) {
  let id;
  do {
    id = Math.trunc(Math.random() * 100);
  } while (seen[id] || id % 10 === 0 || id % 10 === 9 || id >= 90 || id < 10);
  if (id < 10) return `0${id}`;
  return id;
}

function rInit(id, type) {
  return `${prefix}
  DELETE DATA {
    :Cell${id} a :CellFree
  } ;
  INSERT DATA {
    :Cell${id} a :${type}
  }`;
}

async function initDatabase(nSnowman = 1) {
  const seen = {};
  await cleanDatabase();
  for (let n = 0; n < nSnowman; n++) {
    const idSmall = getNewId(seen);
    seen[idSmall] = true;
    await execute(rInit(idSmall, "SmallSnowball"));

    const idMedium = getNewId(seen);
    seen[idMedium] = true;
    await execute(rInit(idMedium, "MediumSnowball"));

    const idBig = getNewId(seen);
    seen[idBig] = true;
    await execute(rInit(idBig, "BigSnowball"));
  }
  const idPlayer = getNewId(seen);
  seen[idPlayer] = true;
  await execute(rInit(idPlayer, "CellPlayer"));
}

function isFinish() {
  return execute(`${prefix}
  SELECT (COUNT(?snowman) as ?nSnowman) (COUNT(?small) as ?nSmall)
  WHERE {
    {
        ?small a :SmallSnowball
    } UNION {
        ?snowman a :SmallSnowball ;
                 a :MediumSnowball ;
                 a :BigSnowball
    }
  }`);
}

function execute(request, reasoning = false, limit = 100) {
  return query.execute(
    conn,
    "snowman",
    request,
    "application/sparql-results+json",
    {
      limit: limit,
      reasoning: reasoning
    }
  );
  //.then(({ body }) => callback(body));
}

module.exports = {
  getBoard() {
    return execute(board);
  },
  makeMove,
  initDatabase,
  isFinish
};
